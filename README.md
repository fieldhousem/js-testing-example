# Very Minimal JS Testing
This project contains the minimum dependencies and configuration required to run
tests using Mocha. Chai is used as an assertion library.

babel-core and babel-preset-env provide the ability to write tests using ES2015+ syntax. 
These dependencies can be removed from package.json if this is not required. '.babelrc' 
can also be deleted.

## Usage
 - `git clone https://gitlab.com/fieldhousem/js-testing-example.git`
 - `npm install`
 - `npm run test -s`

